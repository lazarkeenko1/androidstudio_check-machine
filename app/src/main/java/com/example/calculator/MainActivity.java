package com.example.calculator;

import android.content.Context;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

public class MainActivity extends AppCompatActivity {
    String selection;
    Spinner spinner;
    String[] autoServices = { "Шиншиныч", "Масленок", "Рессора"};
    String item;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        spinner = findViewById(R.id.spinner);
        RadioButton carRadioButton = (RadioButton) findViewById(R.id.carRadioButton);
        RadioButton bikeRadioButton = (RadioButton) findViewById(R.id.bikeRadioButton);
        CheckBox inspectionCheckBox = (CheckBox) findViewById(R.id.inspectionCheckBox);
        CheckBox oilCheckBox = (CheckBox) findViewById(R.id.oilRefillCheckBox);
        Button confirmButton = (Button) findViewById(R.id.confirmButton);
        StringBuilder check = new StringBuilder();
        Context context = getApplicationContext();
        LinearLayout layout = findViewById(R.id.scrollLinearLayout);

        ArrayAdapter<String> adapter = new ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, autoServices);
        spinner.setAdapter(adapter);




        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                item = (String)parent.getItemAtPosition(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        confirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!(carRadioButton.isChecked() || bikeRadioButton.isChecked())){
                    addTextViewToLinearLayout("данные указаны неверно\n", context, layout);
                    return;
                }
                check.append("Заказ:\n");
                check.append("Автосервис: " + (String) spinner.getSelectedItem() + "\n");
                if (carRadioButton.isChecked()) check.append("Транспорт: автомобиль\n");
                if (bikeRadioButton.isChecked()) check.append("Транспорт: мотоцикл\n");
                check.append("Доп опции:\n");
                if (!(inspectionCheckBox.isChecked() || oilCheckBox.isChecked())) check.append("Доп опции не выбраны");
                if (oilCheckBox.isChecked()) check.append("Замена масла");
                if (inspectionCheckBox.isChecked()) check.append("Осмотр тс");
                check.append("\n------------\n");

                addTextViewToLinearLayout(check.toString(), context, layout);

            }
        });


    }
    public boolean onOptionsItemSelected(MenuItem item) {
        // TODO Auto-generated method stub
        Toast.makeText(this, item.getTitle(), Toast.LENGTH_SHORT).show();
        return super.onOptionsItemSelected(item);
    }

    void addTextViewToLinearLayout(String msg, Context context, LinearLayout layout){
        TextView textView = new TextView(context);
        textView.setText(msg);
        textView.setTextSize(30);

        layout.addView(textView, new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT));
    }
}